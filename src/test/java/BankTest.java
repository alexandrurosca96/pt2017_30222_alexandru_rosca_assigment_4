import logic.Account;
import model.Bank;
import model.Person;
import model.SavingAccount;
import model.SpendingAccount;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by Rosca on 07.05.2017.
 */
public class BankTest {
    private Bank bank = new Bank();
    Person person = new Person("Maria", "maria@yahoo.com", 145682);
    Account spendingAccount = new SpendingAccount(564728,1000);
    Account savingAccount = new SavingAccount(747274, 2000);
    double sum = 100;

    public BankTest() throws Exception {
    }

    @Test
    public void addClientTest() throws Exception{
        int sizePre = bank.getSizePersons();
        bank.addPersonAndAccount(person,spendingAccount);
        int sizePost = bank.getSizePersons();
        Assert.assertEquals(sizePre, sizePost - 1);
    }

    @Test
    public void removeClientTest(){
        int sizePre = bank.getSizePersons();
        bank.addPersonAndAccount(person,spendingAccount);
        bank.removePerson(person);
        int sizePost = bank.getSizePersons();
        Assert.assertEquals(sizePre, sizePost );
    }

    @Test
    public void addAccountTest() throws Exception {
        bank.addPersonAndAccount(person,spendingAccount);
        int sizePre = bank.getSizeAccountsForPerson(person);
        bank.addAccountToAPerson(person,savingAccount);

        int sizePost = bank.getSizeAccountsForPerson(person);

        Assert.assertEquals(sizePre + 1, sizePost);
    }

    @Test
    public void addMoneyToSpendingAccount() throws Exception {
        double balancePre = spendingAccount.getBalance();
        spendingAccount.addMoney(sum);

        double balancePost = spendingAccount.getBalance();

        Assert.assertEquals(balancePre + sum, balancePost, 0.001);
    }

    @Test
    public void addMoneyToSavingAccount() throws Exception {
        double balancePre = savingAccount.getBalance();
        savingAccount.addMoney(sum);

        double balancePost = savingAccount.getBalance();

        Assert.assertEquals(balancePre + (1.05*sum), balancePost, 0.001);
    }

    @Test
    public void withDrawMoney() throws Exception {
        double balancePre = spendingAccount.getBalance();
        spendingAccount.withDraw(sum);

        double balancePost = spendingAccount.getBalance();

        Assert.assertEquals(balancePre - sum, balancePost, 0.001);
    }


}
/*
bank.addPersonAndAccount(new Person("Marcel", "asd@yahoo.com", 231231), new SpendingAccount(848231,3000));


 */