package logic;

import logic.Account;
import model.Person;

import java.util.AbstractCollection;
import java.util.HashSet;

/**
 * Created by Rosca on 05.05.2017.
 */
public interface BankProc {

    //public void addPersonAndAccount(Person person, Account account);

    /**
     * @pre person!= null
     * @post getSizePersons()==getSizePersons()@pre + 1
     * @param person
     */
    public void addPeson(Person person) throws Exception;

    /**
     * @pre person != null
     * @pre accounts != null
     * @post getSizePersons() == getSizePersons()@pre + 1
     * @param person
     * @param accounts
     */
    public void addPersonAndAccount(Person person, HashSet<Account> accounts);

    /**
     * @pre person != null
     * @pre getSizePersons() > 0
     * @pre bankListContains(person)
     * @post getSizePersons() == getSizePersons()@pre - 1
     * @param person
     */
    public void removePerson(Person person);

    /**
     * @pre getSizePersons() > 0
     * @pre getSizeAccountsForPerson(person) >= 0
     * @pre person != null
     * @pre account != null
     * @post getSizeAccountsForPerson(person) == getSizeAccountsForPerson(person)@pre + 1
     * @post getSizePersons() == getSizePersons()@pre
     * @param person
     * @param account
     * @throws Exception
     */
    public void addAccountToAPerson(Person person, Account account) throws Exception;

    /**
     * @pre getSizePersons() > 0
     * @pre getSizeAccountsForPerson(person) > 0
     * @pre person != null
     * @pre account != null
     * @post getSizeAccountsForPerson(person) == getSizeAccountsForPerson(person)@pre - 1
     * @post getSizePersons() == getSizePersons()@pre
     * @param person
     * @param account
     */
    public void removeAccount(Person person, Account account);

    /**
     * @pre person != null
     * @pre getSizePersons() > 0
     * @post getSizePersons() == getSizePersons()@pre
     * @param person
     * @return
     */
    public HashSet<Account> readAccountFromPerson(Person person);

    public void loadBankList();

    public void saveBankList();

    /**
     * @pre getSizePersons() > 0
     * @pre getSizeAccountsForPerson(person) > 0
     * @pre person != null
     * @pre sum > 0
     * @pre account != null
     * @post getSizeAccountsForPerson(person) == getSizeAccountsForPerson(person)@pre
     * @post getSizePersons() == getSizePersons()@pre
     * @post account.getBalance() == account.getBalace()@pre + sum
     * @param person
     * @param account
     * @param sum
     */
    public void addMoneyToAccount(Person person, Account account, double sum) throws Exception;

    //public void transferMoney(Account accountSource, Account accountDestination, double sum);
    /**
     * @pre getSizePersons() > 0
     * @pre getSizeAccountsForPerson(person) > 0
     * @pre person != null
     * @pre account.getBalance() > sum
     * @pre sum > 0
     * @pre account != null
     * @post getSizeAccountsForPerson(person) == getSizeAccountsForPerson(person)@pre
     * @post getSizePersons() == getSizePersons()@pre
     * @post account.getBalance() == account.getBalace()@pre - sum
     * @param person
     * @param account
     * @param sum
     */
    public void takeMoneyFromAccount(Person person, Account account, double sum) throws Exception;

}
