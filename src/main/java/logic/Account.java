package logic;

import model.Person;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;
import java.util.Date;
import java.util.IllegalFormatException;
import java.util.NoSuchElementException;
import java.util.Observable;

/**
 * Created by Rosca on 05.05.2017.
 */
public abstract class Account extends Observable implements Serializable {
    protected int accountID;
    protected double balance;

    public Account(int accountID, double balance) throws NoSuchElementException{
        validateAccountID(accountID);
        this.accountID = accountID;
        this.balance = balance;
    }

    public void validateAccountID(int accountID) throws NoSuchElementException{
        if(String.valueOf(accountID).length() != 6) throw new NoSuchElementException("Enter 6 digits for accountID");
    }

    public abstract void addMoney(double sum) throws  Exception;
    public abstract void withDraw(double sum) throws  Exception;

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 31).
                append(accountID).toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if(!(obj instanceof Account)){
            return false;
        }
        if(obj == this){
            return true;
        }
        Account account = (Account) obj;
        return new EqualsBuilder().append(accountID, account.accountID).isEquals();
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public int getAccountID() {
        return accountID;
    }

    public void setAccountID(int accountID) {
        this.accountID = accountID;
    }
}
