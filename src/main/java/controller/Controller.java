package controller;

import logic.Account;
import model.Bank;
import model.Person;
import model.SavingAccount;
import model.SpendingAccount;
import view.View;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.*;

/**
 * Created by Rosca on 05.05.2017.
 */
public class Controller {
    Bank bank;
    View view;
    public Controller(Bank bank, View view){
        this.view = view;
        this.bank = bank;
        view.addActionShowPerson(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                view.setTextArea("");
                try {
                    view.createTablePerson(bank.getPersons());
                    view.setTextArea("Persons's table has been showed!");
                } catch (Exception e1) {
                    view.setTextArea(e1.getMessage());
                }
            }
        });
        view.addWindowAdapter(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                bank.saveBankList();
                System.exit(0);
            }
        });
        view.addActionListenerButtonSelect(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                view.setTextFieldSelectedAccount();
                try {
                    view.setTextArea("Account with : " + view.getSelectedAccount().getAccountID() + " ID has been selected!");
                } catch (Exception e1) {
                    view.setTextArea(e1.getMessage());
                }
            }
        });

        view.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                view.setTextArea("");
                try {
                    String sum = view.selectInsertSumAccount();
                    Account account = view.getSelectedAccount();
                    bank.addMoneyToAccount(view.getSelectedPerson(),account, Double.parseDouble(sum));
                    view.createTableAccount(bank.getAccount(view.getSelectedPerson()));
                    view.setTextFieldSelectedAccount("");
                } catch (Exception e1) {
                    view.setTextArea(e1.getMessage());
                }
            }
        });

        view.addActionListenerWithDraw(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                view.setTextArea("");
                try {
                    String sum = view.selectInsertSumAccount();
                    Account account = view.getSelectedAccount();
                    bank.takeMoneyFromAccount(view.getSelectedPerson(),account, Double.parseDouble(sum));
                    view.createTableAccount(bank.getAccount(view.getSelectedPerson()));
                    view.setTextFieldSelectedAccount("");
                } catch (Exception e1) {
                    view.setTextArea(e1.getMessage());
                }

            }
        });
        view.addActionShowAccount(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                view.setTextArea("");
                Person person = null;
                try {
                person = view.getSelectedPerson();
                    view.createTableAccount(bank.getAccount(person));
                    view.setTextArea("Accounts's table has been showed!");
                }catch (ArrayIndexOutOfBoundsException e2){
                    view.setTextArea(e2.getMessage());
                }
                catch (Exception e1) {
                    view.setTextArea(e1.getMessage());
                }
            }
        });
        view.addActionAddPerson(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                view.setTextArea("");
                try {
                    view.addNewRowPerson();
                    view.setTextArea("A new row for persons has been added!");
                } catch (Exception e1) {
                    view.setTextArea(e1.getMessage());
                }
            }
        });

        view.addActionAddAccount(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                view.setTextArea("");
                try {
                    view.addNewRowAccount();
                    view.setTextArea("A new row for accounts has been added!");
                } catch (Exception e1) {
                    view.setTextArea(e1.getMessage());
                }
            }
        });

        view.addActionRemovePerson(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                view.setTextArea("");
                try {
                Person person = view.getSelectedPerson();
                bank.removePerson(person);
                view.createTablePerson(bank.getPersons());
                view.setTextArea("Person selected has been removed!");

                }catch (ArrayIndexOutOfBoundsException e2){
                    view.setTextArea(e2.getMessage());
                }catch (Exception e1) {
                    view.setTextArea(e1.getMessage());
                }
            }
        });

        view.addActionRemoveAccount(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                view.setTextArea("");
                try {
                    Person person = view.getSelectedPerson();
                    Account account = view.getSelectedAccount();
                    bank.removeAccount(person,account);
                    view.setTextArea("Account selected has been removed!");
                    view.createTableAccount(bank.getAccount(person));
                } catch (Exception e2) {
                    view.setTextArea(e2.getMessage());
                }
            }
        });

        view.addActionEditPerson(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                view.setTextArea("");
                ArrayList<Person> peopleFromBankList = new ArrayList<>();
                ArrayList<Person> peopleFromTable = new ArrayList<>();
                try {
                    for(int row = 0; row < view.modelAccountPerson.getRowCount(); row++){
                        peopleFromTable.add(view.getSelectedPerson(row));
                    }

                    //compare people from table and from bankList to see who is modified
                    peopleFromBankList = bank.getPersons();

                    if(peopleFromBankList.size() != peopleFromTable.size()){ //if the size is diferent that means there is a new person in table
                        Person person = peopleFromTable.get(peopleFromBankList.size() );
                        bank.addPeson(person);
                        view.setTextArea("A new person has been added!");
                    }else{  //one person have been modified
                        for(int index = 0 ; index < peopleFromBankList.size(); index++){
                            if(!peopleFromBankList.get(index).equals(peopleFromTable.get(index))){
                                HashSet<Account> accounts = bank.readAccountFromPerson(peopleFromBankList.get(index));
                                bank.removePerson(peopleFromBankList.get(index));
                                bank.addPersonAndAccount(peopleFromTable.get(index), accounts);
                                view.setTextArea("Person selected has been modified!");
                            }
                        }
                    }
                    view.createTablePerson(bank.getPersons());


                } catch (Exception e1) {
                    view.setTextArea(e1.getMessage());
                }
            }
        });

        view.addActionEditAccount(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                view.setTextArea("");
                ArrayList<Account> accountFromBankList = new ArrayList<>();
                ArrayList<Account> accountFromTable = new ArrayList<>();
                try {
                    for(int row = 0; row < view.modelAccountAccount.getRowCount(); row++) {
                        accountFromTable.add(view.getSelectedAccount(row));
                    }
                    //compare account from table and from bankList to see who is modified
                    accountFromBankList = bank.getAccount(view.getSelectedPerson());

                    if(accountFromBankList.size() != accountFromTable.size()){ //if the size is diferent that means there is a new person in table
                        Account account = accountFromTable.get(accountFromBankList.size());
                        bank.addAccountToAPerson(view.getSelectedPerson(),account);
                        view.setTextArea("A new account has been added!");

                    }else{  //one person have been modified
                        for(int index = 0 ; index < accountFromBankList.size(); index++){
                            if(!accountFromBankList.get(index).equals(accountFromTable.get(index))){
                                bank.removeAccount(view.getSelectedPerson(),accountFromBankList.get(index));
                                bank.addAccountToAPerson(view.getSelectedPerson(), accountFromTable.get(index));
                                view.setTextArea("Account selected has been modified!");
                            }
                        }
                    }
                    view.createTableAccount(bank.getAccount(view.getSelectedPerson()));


                }catch (NoSuchElementException e2 ){
                    view.setTextArea(e2.getMessage());
                }catch (Exception e1) {
                    //e1.printStackTrace();
                    view.setTextArea(e1.getMessage());
                }
            }
        });

    }


}
