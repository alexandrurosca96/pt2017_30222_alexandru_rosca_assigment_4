package controller;

import logic.Account;
import model.Bank;
import model.Person;
import model.SavingAccount;
import model.SpendingAccount;
import view.View;

/**
 * Created by Rosca on 06.05.2017.
 */
public class Main {

    public static void main(String[] args){

        Bank bank = new Bank();
        View view = new View();
        bank.loadBankList();
        Controller controller = new Controller(bank, view);
    }

}
