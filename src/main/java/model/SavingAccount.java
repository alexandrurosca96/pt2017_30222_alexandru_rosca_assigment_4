package model;

import logic.Account;

/**
 * Created by Rosca on 05.05.2017.
 */
public class SavingAccount extends Account{
    private final double interest = 1.05;

    public SavingAccount(int accountID, double balance) throws Exception{
        super(accountID, balance);
    }

    @Override
    public void addMoney(double sum) throws Exception{
        if(sum > 0){
            setBalance((getBalance() + sum*interest));
            setChanged();
            notifyObservers(sum + " lei was added into saving account! ");
        }else{
            throw new Exception("Enter positive sum!");
        }
    }

    @Override
    public void withDraw(double sum) throws Exception{
        if(sum > 0 && (getBalance() - sum) > 0){
            setBalance(getBalance() - sum);
            setChanged();
            notifyObservers(sum + " lei have been withdrawn from saving account! ");
        }else{
            throw new Exception("There are enough money in account!");
        }

    }

    @Override
    public String toString() {
        return "[SavingAccount] id:" + accountID + " balance: " + balance;
    }

    public void withDraw(){
        setBalance(0);
    }

}
