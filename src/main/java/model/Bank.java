package model;

import logic.Account;
import logic.BankProc;

import java.io.*;
import java.util.*;

/**
 * Created by Rosca on 05.05.2017.
 */
public class Bank implements BankProc, Serializable {
    Map<Person, HashSet<Account>> bankList = new HashMap<Person, HashSet<Account>>();

    public void addPersonAndAccount(Person person, Account account) {
        account.addObserver(person);
        assert person != null;
        assert account != null;
        int sizePre = getSizePersons();

        HashSet<Account> accounts = new HashSet<Account>();
        accounts.add(account);
        bankList.put(person,accounts);

        int sizePost = getSizePersons();
        assert sizePost == sizePre + 1;
    }

    @Override
    public void addPeson(Person person) throws Exception{
        assert person != null;
        int sizePre = getSizePersons();

        HashSet<Account> accounts = new HashSet<Account>();
        Account account = new SpendingAccount(100000,0);
        accounts.add(account);
        bankList.put(person,accounts);
        account.addObserver(person);

        int sizePost = getSizePersons();
        assert sizePost == sizePre + 1;
    }

    @Override
    public void addPersonAndAccount(Person person, HashSet<Account> accounts) {
        assert person != null;
        assert accounts != null;
        int sizePre = getSizePersons();

        bankList.put(person,accounts);

        int sizePost = getSizePersons();
        assert sizePost == sizePre + 1;
    }

    public void removePerson(Person person) {
        assert person != null;
        int sizePre = getSizePersons();

        bankList.entrySet().removeIf(e -> (e.getKey()).equals(person));

        int sizePost = getSizePersons();
        assert sizePost == sizePre - 1;
    }

    public void addAccountToAPerson(Person person, Account account) throws Exception {
        assert person != null; assert account != null;
        int sizePre = getSizePersons();
        int sizeAccountPre = getSizeAccountsForPerson(person);

        HashSet<Account> accounts = bankList.get(person);
        if(!accounts.add(account)){
            throw new Exception("Account already exist");
        }
        account.addObserver(person);
        bankList.put(person,accounts);


        int sizePost = getSizePersons();
        int sizeAccountPost = getSizeAccountsForPerson(person);
        assert sizeAccountPost == sizeAccountPre + 1;
        assert sizePost == sizePre;

    }

    public void removeAccount(Person person, Account account) {
        assert person != null; assert account != null;
        int sizePre = getSizePersons();
        int sizeAccountPre = getSizeAccountsForPerson(person);

        HashSet<Account> accounts = bankList.get(person);
        accounts.remove(account);
        bankList.put(person,accounts);

        int sizePost = getSizePersons();
        int sizeAccountPost = getSizeAccountsForPerson(person);
        assert sizeAccountPost == sizeAccountPre - 1;
        assert sizePost == sizePre;
    }

    public HashSet<Account> readAccountFromPerson(Person person){
        return bankList.get(person);
    }
    @Override
    public void loadBankList() {
        HashMap<Person, HashSet<Account>> newBankList = null;
        try {
            FileInputStream fileIn = new FileInputStream("banklist.ser");
            ObjectInputStream in = new ObjectInputStream(fileIn);
            newBankList =(HashMap<Person, HashSet<Account>>) in.readObject();
            this.bankList = newBankList;
            assert wellFormed();
            for(Map.Entry<Person, HashSet<Account>> entry: bankList.entrySet()){
                for(Account account: entry.getValue()){
                    account.addObserver(entry.getKey());
                }
            }
            in.close();
            fileIn.close();
            System.out.println("Bank List was loaded from file!");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void saveBankList() {
        try{
            FileOutputStream fileOut = new FileOutputStream("banklist.ser");
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(bankList);
            out.close();
            fileOut.close();
            System.out.println("Bank List was saved in file!\n");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void addMoneyToAccount(Person person, Account account, double sum) throws Exception {
        HashSet<Account> accounts = bankList.get(person);
        for(Account account1: accounts){
            if(account1.equals(account)){
                account1.addMoney(sum);
            }
        }
        bankList.put(person,accounts);
    }
    /*
    @Override
    public void transferMoney(Account accountSource, Account accountDestination, double sum) {
        for(Map.Entry<Person, HashSet<Account>> entry: bankList.entrySet()){
            for(Account account: entry.getValue()){
                if(account.getAccountID() == accountSource.getAccountID()){
                    account.withDraw(sum);
                }
                if(account.getAccountID() == accountDestination.getAccountID()){
                    account.addMoney(sum);
                }
            }
        }
    }
    */

    @Override
    public void takeMoneyFromAccount(Person person, Account account, double sum) throws Exception {
        double balancePost = 0;
        assert person != null; assert account != null; assert sum > 0;
        double balancePre = account.getBalance();


        HashSet<Account> accounts = bankList.get(person);
        for(Account account1: accounts){
            if(account1.equals(account)){
                account1.withDraw(sum);
                balancePost = account1.getBalance();
            }
        }
        bankList.put(person,accounts);

        assert balancePost == balancePre - sum;
    }

    @Override
    public String toString() {
        String string = "\n";
        for(Map.Entry<Person, HashSet<Account>> entry: bankList.entrySet()){
            string += entry.getKey().toString() + " ---- Has Accounts: \n";
            if(entry.getValue() == null){
                return  string;
            }
            for(Account account: entry.getValue()){
                string += account.toString() + "\n";
            }
            string+="----------------------------------\n";
        }
        return string;
    }

    public ArrayList<Person> getPersons() throws Exception{
        ArrayList<Person> person = new ArrayList<>();
        try {
            for (Map.Entry<Person, HashSet<Account>> entry : bankList.entrySet()) {
                person.add(entry.getKey());
            }
            return person;
        }catch (Exception e){
            throw new Exception("There are no persons in database!");
        }
    }

    public ArrayList<Account> getAccount(Person person) throws Exception{
        assert person != null;
        int sizePre = getSizePersons();

        ArrayList<Account> accountsFinal = new ArrayList<>();
        HashSet<Account> accounts = bankList.get(person);
        try {
            for (Account account : accounts) {
                accountsFinal.add(account);
            }
            int sizePost = getSizePersons();
            assert sizePost == sizePre;
            return accountsFinal;
        }catch (Exception e){
            throw new Exception("There are no Accounts");
        }

    }

    public int getSizePersons(){
        return bankList.size();
    }

    public int getSizeAccountsForPerson(Person person){
        return bankList.get(person).size();
    }

    public boolean wellFormed(){
        int sizePerson = 0;
        int sizeAccount = 0;

        for (Map.Entry<Person, HashSet<Account>> entry : bankList.entrySet()){
            sizePerson++;
            sizeAccount = 0;
            for(Account account : entry.getValue()){
                sizeAccount++;
            }
            if(sizeAccount != entry.getValue().size()){
                return false;
            }
        }
        if(sizePerson != bankList.size()){
            return false;
        }
        return true;
    }

    public Person getPersonwithAccount(Account account) {
        for(Map.Entry<Person, HashSet<Account>> entry: bankList.entrySet()){
            for(Account account1: entry.getValue()){
                if(account1.equals(account)){
                    return entry.getKey();
                }
            }
        }
        return null;
    }
}
