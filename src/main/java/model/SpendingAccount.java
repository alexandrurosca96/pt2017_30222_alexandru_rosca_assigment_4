package model;

import logic.Account;

/**
 * Created by Rosca on 05.05.2017.
 */
public class SpendingAccount extends Account{

    public SpendingAccount(int accountID, double balance)throws Exception{
        super(accountID, balance);
    }

    public void addMoney(double sum) throws Exception {
        if(sum > 0){
            setBalance(getBalance() + sum);
            setChanged();
            notifyObservers(sum + " lei was added into spending account! ");
        }else{
            throw new Exception("Enter positive sum!");
        }
    }

    public void withDraw(double sum) throws Exception {
        if(sum > 0  && (getBalance() - sum) > 0){
            setBalance(getBalance() - sum);
            setChanged();
            notifyObservers(sum + " lei have been withdrawn from spending account! ");
        }else{
            throw new Exception("There are enough money in account!");
        }
    }

    @Override
    public String toString() {
        return "[SpendingAccount] id:" + accountID + " balance: " + balance;
    }
}
