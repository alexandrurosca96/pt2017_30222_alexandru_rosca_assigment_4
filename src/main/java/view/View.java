/*
 * Created by JFormDesigner on Sat May 06 16:50:58 EEST 2017
 */

package view;

import logic.Account;
import model.Bank;
import model.Person;
import model.SavingAccount;
import model.SpendingAccount;

import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.lang.reflect.Field;
import java.util.*;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;

/**
 * @author Alexandru Rosca
 */
public class View extends JFrame {
      public View() {
        initComponents();
        this.setVisible(true);
        this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - Alexandru Rosca
        panel1 = new JPanel();
        label8 = new JLabel();
        buttonShowPersons = new JButton();
        button1 = new JButton();
        buttonEditPerson = new JButton();
        buttonRemovePerson = new JButton();
        panel2 = new JPanel();
        label9 = new JLabel();
        buttonShowAccount = new JButton();
        buttonAccount = new JButton();
        buttonEditAccount = new JButton();
        buttonRemoveAccount = new JButton();
        scrollPane1 = new JScrollPane();
        tablePerson = new JTable();
        scrollPane2 = new JScrollPane();
        tableAccount = new JTable();
        label1 = new JLabel();
        textFieldSum = new JTextField();
        buttonAdd = new JButton();
        label2 = new JLabel();
        textFieldSelectedAccount = new JTextField();
        buttonWithDraw = new JButton();
        label3 = new JLabel();
        label4 = new JLabel();
        textFieldFrom = new JTextField();
        label5 = new JLabel();
        textFieldTo = new JTextField();
        label6 = new JLabel();
        textFieldSumTransfer = new JTextField();
        buttonTransfer = new JButton();
        label7 = new JLabel();
        buttonSelect = new JButton();
        textArea = new JTextArea();

        //======== this ========
        setForeground(Color.pink);
        setBackground(new Color(204, 255, 255));
        Container contentPane = getContentPane();
        contentPane.setLayout(null);

        //======== panel1 ========
        {
            panel1.setLayout(null);

            //---- label8 ----
            label8.setText("Person");
            label8.setFont(new Font("Segoe UI", Font.BOLD, 16));
            panel1.add(label8);
            label8.setBounds(new Rectangle(new Point(125, 5), label8.getPreferredSize()));

            //---- buttonShowPersons ----
            buttonShowPersons.setText("Show ");
            panel1.add(buttonShowPersons);
            buttonShowPersons.setBounds(new Rectangle(new Point(5, 30), buttonShowPersons.getPreferredSize()));

            //---- button1 ----
            button1.setText("Add ");
            panel1.add(button1);
            button1.setBounds(new Rectangle(new Point(85, 30), button1.getPreferredSize()));

            //---- buttonEditPerson ----
            buttonEditPerson.setText("Edit");
            panel1.add(buttonEditPerson);
            buttonEditPerson.setBounds(new Rectangle(new Point(150, 30), buttonEditPerson.getPreferredSize()));

            //---- buttonRemovePerson ----
            buttonRemovePerson.setText("Remove");
            panel1.add(buttonRemovePerson);
            buttonRemovePerson.setBounds(new Rectangle(new Point(220, 30), buttonRemovePerson.getPreferredSize()));

            { // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel1.getComponentCount(); i++) {
                    Rectangle bounds = panel1.getComponent(i).getBounds();
                    preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                    preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel1.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel1.setMinimumSize(preferredSize);
                panel1.setPreferredSize(preferredSize);
            }
        }
        contentPane.add(panel1);
        panel1.setBounds(5, 5, 305, 85);

        //======== panel2 ========
        {
            panel2.setLayout(null);

            //---- label9 ----
            label9.setText("Account");
            label9.setFont(new Font("Segoe UI", Font.BOLD, 16));
            panel2.add(label9);
            label9.setBounds(new Rectangle(new Point(125, 5), label9.getPreferredSize()));

            //---- buttonShowAccount ----
            buttonShowAccount.setText("Show ");
            panel2.add(buttonShowAccount);
            buttonShowAccount.setBounds(new Rectangle(new Point(5, 30), buttonShowAccount.getPreferredSize()));

            //---- buttonAccount ----
            buttonAccount.setText("Add ");
            panel2.add(buttonAccount);
            buttonAccount.setBounds(new Rectangle(new Point(90, 30), buttonAccount.getPreferredSize()));

            //---- buttonEditAccount ----
            buttonEditAccount.setText("Edit");
            panel2.add(buttonEditAccount);
            buttonEditAccount.setBounds(new Rectangle(new Point(160, 30), buttonEditAccount.getPreferredSize()));

            //---- buttonRemoveAccount ----
            buttonRemoveAccount.setText("Remove");
            panel2.add(buttonRemoveAccount);
            buttonRemoveAccount.setBounds(new Rectangle(new Point(225, 30), buttonRemoveAccount.getPreferredSize()));

            { // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel2.getComponentCount(); i++) {
                    Rectangle bounds = panel2.getComponent(i).getBounds();
                    preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                    preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel2.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel2.setMinimumSize(preferredSize);
                panel2.setPreferredSize(preferredSize);
            }
        }
        contentPane.add(panel2);
        panel2.setBounds(320, 5, 305, 85);

        //======== scrollPane1 ========
        {
            scrollPane1.setViewportView(tablePerson);
        }
        contentPane.add(scrollPane1);
        scrollPane1.setBounds(20, 90, 280, 185);
        //JOptionPane.showMessageDialog(null,new JScrollPane(tablePerson));

        //======== scrollPane2 ========
        {
            scrollPane2.setViewportView(tableAccount);
        }
        contentPane.add(scrollPane2);
        scrollPane2.setBounds(340, 90, 270, 185);

        //---- label1 ----
        label1.setText("Insert amount:");
        contentPane.add(label1);
        label1.setBounds(500, 315, label1.getPreferredSize().width, 20);
        contentPane.add(textFieldSum);
        textFieldSum.setBounds(582, 315, 37, 20);

        //---- buttonAdd ----
        buttonAdd.setText("Add Money");
        contentPane.add(buttonAdd);
        buttonAdd.setBounds(new Rectangle(new Point(365, 345), buttonAdd.getPreferredSize()));

        //---- label2 ----
        label2.setText("Selected Account: ");
        contentPane.add(label2);
        label2.setBounds(new Rectangle(new Point(310, 315), label2.getPreferredSize()));
        contentPane.add(textFieldSelectedAccount);
        textFieldSelectedAccount.setBounds(420, 315, 75, 20);

        //---- buttonWithDraw ----
        buttonWithDraw.setText("WithDraw");
        contentPane.add(buttonWithDraw);
        buttonWithDraw.setBounds(480, 345, 92, 32);



        textFieldSelectedAccount.setEditable(false);
        //--Select

        buttonSelect.setText("Select");
        contentPane.add(buttonSelect);
        buttonSelect.setBounds(new Rectangle(new Point(285, 277), buttonSelect.getPreferredSize()));

        //--Text area

        contentPane.add(textArea);
        textArea.setBounds(20, 307, 275,60);
        textArea.setEditable(false);
        textArea.setVisible(true);

        //---- label7 ----
        label7.setText("Account Operation");
        label7.setFont(new Font("Segoe UI", Font.PLAIN, 14));
        contentPane.add(label7);
        label7.setBounds(new Rectangle(new Point(435, 290), label7.getPreferredSize()));

        { // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < contentPane.getComponentCount(); i++) {
                Rectangle bounds = contentPane.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = contentPane.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            contentPane.setMinimumSize(preferredSize);
            contentPane.setPreferredSize(preferredSize);
        }
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }
    // JFormDesigner - End of variables declaration  //GEN-S:variables
    public DefaultTableModel modelAccountPerson, modelAccountAccount;
    private JPanel panel1;
    private JLabel label8;
    private JButton buttonShowPersons;
    private JButton button1;
    private JButton buttonEditPerson;
    private JButton buttonRemovePerson;
    private JPanel panel2;
    private JLabel label9;
    private JButton buttonShowAccount;
    private JButton buttonAccount;
    private JButton buttonEditAccount;
    private JButton buttonRemoveAccount;
    private JScrollPane scrollPane1;
    public JTable tablePerson = new JTable(modelAccountPerson) ;
    private JScrollPane scrollPane2;
    public JTable tableAccount = new JTable(modelAccountAccount);
    private JLabel label1;
    private JTextField textFieldSum;
    private JButton buttonAdd;
    private JLabel label2;
    private JTextField textFieldSelectedAccount;
    private JButton buttonWithDraw;
    private JLabel label3;
    private JLabel label4;
    private JTextField textFieldFrom;
    private JLabel label5;
    private JTextField textFieldTo;
    private JLabel label6;
    private JTextField textFieldSumTransfer;
    private JButton buttonTransfer;
    private JLabel label7;
    private JButton buttonSelect;
    private JTextArea textArea;


    public void addActionShowPerson(ActionListener al){
        this.buttonShowPersons.addActionListener(al);
    }
    public void addActionAddPerson(ActionListener al){
        button1.addActionListener(al);
    }
    public void addActionEditPerson(ActionListener al){
        buttonEditPerson.addActionListener(al);
    }
    public void addActionRemovePerson(ActionListener al){
        buttonRemovePerson.addActionListener(al);
    }

    public void addActionShowAccount(ActionListener al){
        this.buttonShowAccount.addActionListener(al);
    }
    public void addActionAddAccount(ActionListener al){
        buttonAccount.addActionListener(al);
    }
    public void addActionEditAccount(ActionListener al){
        buttonEditAccount.addActionListener(al);
    }
    public void addActionRemoveAccount(ActionListener al){
        buttonRemoveAccount.addActionListener(al);
    }
    public void addActionListener(ActionListener al){
        buttonAdd.addActionListener(al);
    }
    public void addActionListenerWithDraw(ActionListener al){
        buttonWithDraw.addActionListener(al);
    }
    public void addWindowAdapter(WindowAdapter windowAdapter){
        this.addWindowListener(windowAdapter);
    }

    public void addActionListenerButtonSelect(ActionListener al){
        buttonSelect.addActionListener(al);
    }

    public String selectInsertSumAccount()throws  Exception{
        if(textFieldSum.getText().equals("")){
            throw new Exception("Enter sum!");
        }else{
            return textFieldSum.getText();
        }

    }

    public void setTextFieldSelectedAccount(){
        try {
            textFieldSelectedAccount.setText(Integer.toString( getSelectedAccount().getAccountID()));
        } catch (Exception e) {
            textFieldSelectedAccount.setText(Integer.toString(0));
        }
    }

    public void setTextFieldSelectedAccount(String s){
       textFieldSelectedAccount.setText(s);
    }
    public void setTextArea(String string){
        textArea.setText(string);
    }

    public Person getSelectedPerson() throws  Exception{
        try {
            String name = tablePerson.getValueAt(tablePerson.getSelectedRow(), 0).toString();
            String email = (String) tablePerson.getValueAt(tablePerson.getSelectedRow(), 1).toString();
            long cnp = Long.parseLong(tablePerson.getValueAt(tablePerson.getSelectedRow(), 2).toString());
            Person person = new Person(name, email, cnp);
            return person;
        }catch( ArrayIndexOutOfBoundsException e1){
            throw new ArrayIndexOutOfBoundsException("Select a person!");
        }
    }

    public Person getSelectedPerson(int row) throws  Exception{
        try {
            long cnp;
            String name = tablePerson.getValueAt(row, 0).toString();
            if(name.equals("")){
                throw new Exception("Fill al the fields!");
            }
            String email = (String) tablePerson.getValueAt(row, 1).toString();
            cnp = Long.parseLong(tablePerson.getValueAt(row, 2).toString());

            Person person = new Person(name, email, cnp);
            return person;
        }catch( ArrayIndexOutOfBoundsException e1){
            throw new ArrayIndexOutOfBoundsException("Select a person!");
        }
    }

    public Account getSelectedAccount() throws NoSuchElementException, Exception{
        try{
            String typeOfAccount = tableAccount.getValueAt(tableAccount.getSelectedRow(),2).toString();
            int accountID = Integer.parseInt(tableAccount.getValueAt(tableAccount.getSelectedRow(),0).toString());
            double balance = Double.parseDouble(tableAccount.getValueAt(tableAccount.getSelectedRow(),1).toString());
            if(typeOfAccount.equals("Spending Account")){
                SpendingAccount spendingAccount = new SpendingAccount(accountID, balance);
                return spendingAccount;
            }else{
                SavingAccount savingAccount = new SavingAccount(accountID, balance);
                return savingAccount;
            }
        }catch (NoSuchElementException e1){
            throw new NoSuchElementException(e1.getMessage());
        }
        catch(Exception e){
            throw new Exception("Select Account!");
        }
    }

    public Account getSelectedAccount(int row) throws NoSuchElementException , Exception{
        try{
            String typeOfAccount = tableAccount.getValueAt(row,2).toString();
            int accountID = Integer.parseInt(tableAccount.getValueAt(row,0).toString());
            double balance = Double.parseDouble(tableAccount.getValueAt(row,1).toString());
            if(typeOfAccount.equals("Spending Account")){
                SpendingAccount spendingAccount = new SpendingAccount(accountID, balance);
                return spendingAccount;
            }else{
                SavingAccount savingAccount = new SavingAccount(accountID, balance);
                return savingAccount;
            }
        }catch (NoSuchElementException e1){
            throw new NoSuchElementException(e1.getMessage());
        }
        catch(Exception e){
            throw new Exception("Select Account!");
        }
    }

    public void addNewRowPerson() throws Exception{
        try {
            modelAccountPerson.addRow(new Object[]{"", "", ""});
        }catch (Exception e){
            throw new Exception("Press to show the Person table first!");
        }
    }

    public void addNewRowAccount() throws  Exception{
        try {
            modelAccountAccount.addRow(new Object[]{"", "", ""});
        }catch (Exception e1){
            throw new Exception("Press to show the Account table fist!");
        }
    }

    public void createTablePerson(java.util.List<Person> list) {
        Vector<String> columnNames = new Vector<String>();
        Vector<Object> dataForEachObject = new Vector<Object>();

        columnNames.add("name");
        columnNames.add("email");
        columnNames.add("cnp");

         modelAccountPerson = new DefaultTableModel(null, columnNames) {
            public boolean isCellEditable(int row, int column) {
                return true;
            }
        };

        for(Person person: list) {         //for each object from list
            dataForEachObject.add(person.getName());
            dataForEachObject.add(person.getEmail());
            dataForEachObject.add(person.getCnp());

            //data.addElement(dataForEachObject);

            modelAccountPerson.addRow((Vector) dataForEachObject.clone());
            dataForEachObject.removeAllElements();
        }

        tablePerson.setModel(modelAccountPerson);
    }

    public void createTableAccount(java.util.List<Account> list) {
        Vector<String> columnNames = new Vector<String>();
        Vector<Object> dataForEachObject = new Vector<Object>();
        // Vector<Vector<Object>> data = new Vector<Vector<Object>>();

        columnNames.add("accountID");
        columnNames.add("balance");
        columnNames.add("tip");

        modelAccountAccount = new DefaultTableModel(null, columnNames) {
            public boolean isCellEditable(int row, int column) {
                return true;
            }
        };

        for(Account account: list) {         //for each object from list
            dataForEachObject.add(account.getAccountID());
            dataForEachObject.add(account.getBalance());
            if(account instanceof SpendingAccount){
                dataForEachObject.add("Spending Account");
            }else{
                dataForEachObject.add("Saving Account");
            }
            modelAccountAccount.addRow((Vector) dataForEachObject.clone());
            dataForEachObject.removeAllElements();
        }

        tableAccount.setModel(modelAccountAccount);
    }
}
